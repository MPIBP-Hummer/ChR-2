**************************************************
Copyright (c) 2017 Albert Ardevol and Gerhard Hummer, Max-Planck Institute of 
Biophysics, Frankfurt am Main, Germany 

Permission is hereby granted, free of charge, to any person obtaining a copy
of these force field parameters and associated documentation files, to deal
in the parameters without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the parameters, and to permit persons to whom the parameters is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the parameters.

THE FORCE-FIELD PARAMETERS ARE PROVIDED "AS ARE", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE FORCE-FIELD PARAMETERS OR THE USE OR OTHER 
DEALINGS IN THE FORCE-FIELD PARAMETERS.
**************************************************

AMBER (www.ambermd.org) preparation and parameters files for protonated retinal Schiff 
base to be used in Channelrhodopsin-2
